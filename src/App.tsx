import React, { useState, useEffect } from "react";
import "./App.css";
import axios from "axios";
import InfiniteScroll from "react-infinite-scroll-component";

function App() {
  const [data, setData] = useState([] as any);
  const [page, setPage] = useState(1);

  useEffect(() => {
    const nextData = async () => {
      await axios
        .get(`https://jsonplaceholder.typicode.com/comments?postId=${page}`)
        .then(res => {
          setData([...data, ...res.data]);
        })
        .catch(err => console.log(err));
    };
    nextData();
  }, [page]);

  return (
    <div className='App'>
      <h1>My Blog</h1>

      <div className='container'>
        <InfiniteScroll dataLength={data.length} next={() => setPage(page + 1)} loader={<p>please wait....</p>} hasMore={true}>
          {data.map((posts: any, key: any) => (
            <div key={key}>
              <h1>{posts.name}</h1>
              <p>{posts.body}</p>
            </div>
          ))}
        </InfiniteScroll>
      </div>
    </div>
  );
}

export default App;
